package com.example.mohammedmobinmunir.myapplication.dagger

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by MohammedMobinMunir on 3/5/2018.
 */
@Module
class ProviderModule {

    @Provides
    fun getGSON(): Gson = Gson()

    @Provides
    fun getRetrofit(): Retrofit =
            Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl("https://newsapi.org/v2/").build()


}