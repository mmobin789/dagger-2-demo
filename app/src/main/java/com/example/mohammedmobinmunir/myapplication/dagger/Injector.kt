package com.example.mohammedmobinmunir.myapplication.dagger

import com.example.mohammedmobinmunir.myapplication.ApiCall
import com.example.mohammedmobinmunir.myapplication.MainActivity
import dagger.Component
import javax.inject.Singleton

/**
 * Created by MohammedMobinMunir on 3/5/2018.
 */
@Singleton
@Component(modules = [ProviderModule::class])
interface Injector {
    fun inject(mainActivity: MainActivity)
    fun inject(apiCall: ApiCall)
}