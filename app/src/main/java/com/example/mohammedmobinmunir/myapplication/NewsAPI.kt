package com.example.mohammedmobinmunir.myapplication

import retrofit2.Call
import retrofit2.http.GET

/**
 * Created by MohammedMobinMunir on 3/5/2018.
 */
interface NewsAPI {
    @GET("top-headlines?sources=google-news&apiKey=1e963d8f38024b4e98f406d0ed337ffc")
    fun getNews(): Call<News>
}