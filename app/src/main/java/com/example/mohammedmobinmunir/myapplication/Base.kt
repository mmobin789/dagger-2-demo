package com.example.mohammedmobinmunir.myapplication

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.mohammedmobinmunir.myapplication.dagger.DaggerInjector
import com.example.mohammedmobinmunir.myapplication.dagger.Injector
import com.example.mohammedmobinmunir.myapplication.dagger.ProviderModule


/**
 * Created by MohammedMobinMunir on 3/5/2018.
 */
abstract class Base : AppCompatActivity() {

    companion object {


        lateinit var injector: Injector
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injector = DaggerInjector.builder().providerModule(ProviderModule()).build()

    }

}