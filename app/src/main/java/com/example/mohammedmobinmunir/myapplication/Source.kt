package com.example.mohammedmobinmunir.myapplication

data class Source(
        val name: String = "",
        val id: String = ""
)
