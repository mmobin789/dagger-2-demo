package com.example.mohammedmobinmunir.myapplication

import android.os.Bundle
import android.util.Log

class MainActivity : Base() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val api = ApiCall.getInstance()
        api.getNews()
        Log.i("Dagger 2", "A Fine Example See you !!")

    }
}
