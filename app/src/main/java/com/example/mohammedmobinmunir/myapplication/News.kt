package com.example.mohammedmobinmunir.myapplication

data class News(
        val totalResults: Int = -1,
        val articles: List<ArticlesItem> = emptyList(),
        val status: String = ""
)
