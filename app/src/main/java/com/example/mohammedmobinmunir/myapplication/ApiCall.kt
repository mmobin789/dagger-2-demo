package com.example.mohammedmobinmunir.myapplication

import android.util.Log
import com.example.mohammedmobinmunir.myapplication.Base.Companion.injector
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

/**
 * Created by MohammedMobinMunir on 3/5/2018.
 */
class ApiCall {
    init {
        injector.inject(this)
    }

    companion object {
        private var apiCall: ApiCall? = null
        fun getInstance(): ApiCall {
            if (apiCall == null)
                apiCall = ApiCall()
            return apiCall!!
        }
    }

    @Inject
    lateinit var retrofit: Retrofit
    @Inject
    lateinit var gson: Gson

    private fun getAPI(): NewsAPI = retrofit.create(NewsAPI::class.java)


    fun getNews() {
        getAPI().getNews().enqueue(object : Callback<News> {
            override fun onFailure(call: Call<News>?, t: Throwable?) {
                Log.e("NewsAPI", t.toString())

            }

            override fun onResponse(call: Call<News>?, response: Response<News>?) {
                Log.i("NewsAPI", gson.toJson(response!!.body()))
            }
        })
    }
}